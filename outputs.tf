
output "k8_access_role" {
  description = "A role to allow k8 pod to access the AWS account"
  value = var.create ? aws_iam_role.k8_access_role.0 : null
}