# K8 Service Account Role

## Inputs

The following inputs are available the first is optional the rest are required.

```terraform
variable "create" {
  description = "The service account name for which the role must be created"
  type        = bool
  default     = true
}
```

```terraform
variable "k8_oidc_provider_url" {
  description = "The k8 oidc provider url without the https"
  type        = string
  default     = "oidc.eks.us-east-1.amazonaws.com/id/0488F38C8F97CB5B49EA6B41CE9BDA02"
}
```

```terraform
variable "k8_namespace" {
  description = "The k8 namespace for which the role will allow access"
  type        = string
}
```

```terraform
variable "k8_service_account_name" {
  description = "The service account name for which the role must be created"
  type        = string
}
```

# Outputs

```
output.k8_access_role.id
output.k8_access_role.arn
```