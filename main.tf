data "aws_caller_identity" "current" {}
data "aws_partition" "current" {}

data "aws_iam_policy_document" "oidc_trust_relationship" {
  statement {
    sid     = "AllowClusterAccess"
    effect  = "Allow"
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type = "Federated"
      identifiers = [
        "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${var.k8_oidc_provider_url}"
      ]
    }

    condition {
      test     = "StringEquals"
      variable = "${var.k8_oidc_provider_url}:sub"
      values = [
        "system:serviceaccount:${var.k8_namespace}:${var.k8_service_account_name}"
      ]
    }
  }
}

resource "aws_iam_role" "k8_access_role" {
  name               = var.k8_service_account_name
  count              = var.create ? 1 : 0
  description        = "A role to allow K8 service account (${var.k8_service_account_name}) access from cluster to AWS account"
  assume_role_policy = data.aws_iam_policy_document.oidc_trust_relationship.json
}
