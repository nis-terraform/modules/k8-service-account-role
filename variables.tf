variable "create" {
  description = "If a falsey is provided then do not create this resource"
  type        = bool
  default     = true
}

variable "k8_oidc_provider_url" {
  description = "The k8 oidc provider url without the https"
  type        = string
  default     = "oidc.eks.us-east-1.amazonaws.com/id/0488F38C8F97CB5B49EA6B41CE9BDA02"
}

variable "k8_namespace" {
  description = "The k8 namespace for which the role will allow access"
  type        = string
}

variable "k8_service_account_name" {
  description = "The service account name for which the role must be created"
  type        = string
}